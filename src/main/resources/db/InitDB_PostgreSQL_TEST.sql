DROP TABLE IF EXISTS products;
DROP SEQUENCE IF EXISTS global_seq;

CREATE SEQUENCE global_seq START WITH 1;

CREATE TABLE products
(
    id          INTEGER PRIMARY KEY,
    name        VARCHAR(255) UNIQUE NOT NULL,
    description VARCHAR(255)        NOT NULL
);
INSERT INTO products (id, name, description)
VALUES (1, 'Молоко', 'описание продукта'),
       (2, 'Творог', 'описание продукта'),
       (3, 'Сливки', 'описание продукта'),
       (4, 'Пиво', 'описание продукта'),
       (5, 'Сок', 'описание продукта'),
       (6, 'Хлопья', 'описание продукта'),
       (7, 'Масло сливочное', 'описание продукта'),
       (8, 'Сыр', 'описание продукта'),
       (9, 'Сметана', 'описание продукта'),
       (10, 'Йогурт', 'описание продукта');